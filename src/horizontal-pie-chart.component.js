/**
 * Created by richard on 17/10/16.
 */
/*global angular, $*/
'use strict';

//noinspection JSUnusedGlobalSymbols,JSUnresolvedFunction
angular.module('bitcraft-horizontal-pie-chart')
    .component('bitcraftHorizontalPieChart', {
        templateUrl: './js/bitcraft-horizontal-pie-chart/bitcraft-horizontal-pie-chart.template.html',
        controller: [
            '$scope', '$timeout', '$window',
            function ($scope, $timeout, $window) {
                var self = this;

                var normalizeData = function () {
                    var i;
                    var totValue = 0;
                    var totWidthGiven = 0;
                    var node;
                    for (i = 0; i < self.data.length; i = i + 1) {
                        totValue += parseInt(self.data[i].value, 10);
                    }

                    for (i = 0; i < self.data.length - 1; i = i + 1) {
                        node = self.data[i];
                        node.width = Math.round(self.options.width * parseInt(node.value, 10) / totValue);
                        totWidthGiven += node.width;
                    }

                    node = self.data[i];
                    node.width = self.options.width - totWidthGiven;
                };

                var parseOptions = function () {
                    var defaultOptions = {
                        title: 'Graph',
                        width: 400,
                        height: 30,
                        valueSuffix: '',
                        showLegend: true
                    };

                    var defaultTextColor = '#000000';
                    var defaultColors = ['#ffcfa0', '#4d81d6', '#e52d2d'];

                    var i, key;

                    for (key in defaultOptions) {
                        if (defaultOptions.hasOwnProperty(key)) {
                            self.options[key] = self.options[key] || defaultOptions[key];
                        }
                    }

                    for (i = 0; i < self.data.length; i = i + 1) {
                        self.data[i].textColor = self.data[i].textColor || defaultTextColor;
                        self.data[i].color = self.data[i].color || defaultColors[i % (defaultColors.length)];
                    }
                };

                var canBeDisplayedRight = function (top, left) {
                    return left + $scope.width < $window.innerWidth;
                };

                var initTooltip = function () {
                    var horizontalPieData = $('.horizontal-pie-data');

                    horizontalPieData.mouseenter(function (event) {
                        var target = $(event.target);
                        if (!target.hasClass('horizontal-pie-data')) {
                            target = target.parents('.horizontal-pie-data');
                        }

                        var tooltip =  target.children('.horizontal-pie-tooltip').first();
                        tooltip.css('display', 'inline-block');

                        var width = tooltip.outerWidth();
                        $scope.width = Math.max($scope.width, width);

                        if (canBeDisplayedRight(event.clientY, event.clientX + 40)) {
                            tooltip.css('top', event.clientY - 10);
                            tooltip.css('left', event.clientX + 40);
                        } else {
                            tooltip.css('top', event.clientY - 10);
                            tooltip.css('left', event.clientX - 20 - width);
                        }
                    });
                    horizontalPieData.mousemove(function (event) {
                        var target = $(event.target);
                        if (!target.hasClass('horizontal-pie-data')) {
                            target = target.parents('.horizontal-pie-data');
                        }

                        var tooltip =  target.children('.horizontal-pie-tooltip').first();
                        var width = tooltip.outerWidth();
                        $scope.width = Math.max($scope.width, width);

                        if (canBeDisplayedRight(event.clientY, event.clientX + 40)) {
                            tooltip.css('top', event.clientY - 10);
                            tooltip.css('left', event.clientX + 40);
                        } else {
                            tooltip.css('top', event.clientY - 10);
                            tooltip.css('left', event.clientX - 20 - width);
                        }
                    });
                    horizontalPieData.mouseleave(function (event) {
                        $scope.width = 0;
                        var target = $(event.target);

                        // handle the case we 'stepped' on the tooltip
                        if (!target.hasClass('horizontal-pie-data')) {
                            target = target.parents('.horizontal-pie-data');
                        }
                        target.off('mouvemove');
                        target.children('.horizontal-pie-tooltip').first().css('display', 'none');
                    });
                };

                this.$onInit = function () {
                    if (!this.options || !this.data) {
                        console.error('No options or no data passed.');
                        return;
                    }

                    parseOptions();
                    normalizeData();

                    $scope.width = 0;
                    $timeout(initTooltip);

                    $scope.$watch(function () { return self.data; }, normalizeData, true);
                };
            }
        ],
        bindings: {
            options: '<',
            data: '<'
        }
});
