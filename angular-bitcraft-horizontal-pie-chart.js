(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by richard on 17/10/16.
 */
/*global angular, $*/
'use strict';

var template = '' +
'<style>' +
'    .horizontal-pie-data-wrap {' +
'        border: solid #ccc 1px;' +
'        border-radius: 2px;' +
'        box-sizing: content-box;' +
'        width: {{$ctrl.options.width}}px;' +
'        height: {{$ctrl.options.height}}px;' +
'        line-height: {{$ctrl.options.height}}px;' +
'    }' +
'    .horizontal-pie-data {' +
'        float: left;' +
'        text-align: center;' +
'        overflow: hidden;' +
'        text-overflow: ellipsis;' +
'    }' +
'    .horizontal-pie-value {' +
'        overflow: hidden;' +
'        text-overflow: ellipsis;' +
'        white-space: nowrap;' +
'    }' +
'    .horizontal-pie-tooltip {' +
'        height: 30px;' +
'        line-height: 30px;' +
'        background-color: white;' +
'        border-radius: 5px;' +
'        border: solid #666 1px;' +
'    }' +
'    .horizontal-pie-tooltip > div {' +
'        float: left;' +
'    }' +
'    .horizontal-pie-color-viewer {' +
'        margin: 8px;' +
'        width: 12px;' +
'        height: 12px;' +
'        border: solid #ccc 1px;' +
'    }' +
'    .horizontal-pie-title {' +
'        text-align: center;' +
'        font-size: 1.25em;' +
'        width: {{$ctrl.options.width}}px;' +
'    }' +
'    .horizontal-pie-legend {' +
'        margin-top: 3px;' +
'        display: inline-block;' +
'        /*border: solid #ccc 1px;*/' +
'        box-sizing: content-box;' +
'        height: 30px;' +
'        line-height: 30px;' +
'        width: {{$ctrl.options.width}}px;' +
'    }' +
'    .horizontal-pie-legend div {' +
'        float: left;' +
'    }' +
'</style>' +
'<div ng-if="$ctrl.data">' +
'    <div class="horizontal-pie-title">{{$ctrl.options.title}} ({{$ctrl.options.valueSuffix}})</div>' +
'    <div class="horizontal-pie-data-wrap">' +
'        <div class="horizontal-pie-data" ng-repeat="d in $ctrl.data" style="background-color: {{d.color}}; width: {{d.width}}px; height: 100%">' +
'            <div class="horizontal-pie-value" style="color: {{d.textColor;}}">{{d.legend}}: {{d.value}}{{$ctrl.options.valueSuffix}}</div>' +
'            <div class="horizontal-pie-tooltip" style="display: none; position: fixed;">' +
'                <div class="horizontal-pie-color-viewer" style="background-color: {{d.color}}"></div>' +
'                <div style="margin-right: 10px">{{d.legend}}: {{d.value}}{{$ctrl.options.valueSuffix}}</div>' +
'            </div>' +
'        </div>' +
'    </div>' +
'    <div ng-if="$ctrl.options.showLegend" class="horizontal-pie-legend">' +
'        <div ng-repeat="d in $ctrl.data">' +
'            <div class="horizontal-pie-color-viewer" style="background-color: {{d.color}}"></div>' +
'            <div style="margin-right: 10px">{{d.legend}}</div>' +
'        </div>' +
'    </div>' +
'</div>' +
'';


//noinspection JSUnusedGlobalSymbols,JSUnresolvedFunction
angular.module('bitcraft-horizontal-pie-chart')
    .component('bitcraftHorizontalPieChart', {
        template: template,
        controller: [
            '$scope', '$timeout', '$window',
            function ($scope, $timeout, $window) {
                var self = this;

                var normalizeData = function () {
                    var i;
                    var totValue = 0;
                    var totWidthGiven = 0;
                    var node;
                    for (i = 0; i < self.data.length; i = i + 1) {
                        totValue += parseInt(self.data[i].value, 10);
                    }

                    for (i = 0; i < self.data.length - 1; i = i + 1) {
                        node = self.data[i];
                        node.width = Math.round(self.options.width * parseInt(node.value, 10) / totValue);
                        totWidthGiven += node.width;
                    }

                    node = self.data[i];
                    node.width = self.options.width - totWidthGiven;
                };

                var parseOptions = function () {
                    var defaultOptions = {
                        title: 'Graph',
                        width: 400,
                        height: 30,
                        valueSuffix: '',
                        showLegend: true
                    };

                    var defaultTextColor = '#000000';
                    var defaultColors = ['#ffcfa0', '#4d81d6', '#e52d2d'];

                    var i, key;

                    for (key in defaultOptions) {
                        if (defaultOptions.hasOwnProperty(key)) {
                            self.options[key] = self.options[key] || defaultOptions[key];
                        }
                    }

                    for (i = 0; i < self.data.length; i = i + 1) {
                        self.data[i].textColor = self.data[i].textColor || defaultTextColor;
                        self.data[i].color = self.data[i].color || defaultColors[i % (defaultColors.length)];
                    }
                };

                var canBeDisplayedRight = function (top, left) {
                    return left + $scope.width < $window.innerWidth;
                };

                var initTooltip = function () {
                    var horizontalPieData = $('.horizontal-pie-data');

                    horizontalPieData.mouseenter(function (event) {
                        var target = $(event.target);
                        if (!target.hasClass('horizontal-pie-data')) {
                            target = target.parents('.horizontal-pie-data');
                        }

                        var tooltip =  target.children('.horizontal-pie-tooltip').first();
                        tooltip.css('display', 'inline-block');

                        var width = tooltip.outerWidth();
                        $scope.width = Math.max($scope.width, width);

                        if (canBeDisplayedRight(event.clientY, event.clientX + 40)) {
                            tooltip.css('top', event.clientY - 10);
                            tooltip.css('left', event.clientX + 40);
                        } else {
                            tooltip.css('top', event.clientY - 10);
                            tooltip.css('left', event.clientX - 20 - width);
                        }
                    });
                    horizontalPieData.mousemove(function (event) {
                        var target = $(event.target);
                        if (!target.hasClass('horizontal-pie-data')) {
                            target = target.parents('.horizontal-pie-data');
                        }

                        var tooltip =  target.children('.horizontal-pie-tooltip').first();
                        var width = tooltip.outerWidth();
                        $scope.width = Math.max($scope.width, width);

                        if (canBeDisplayedRight(event.clientY, event.clientX + 40)) {
                            tooltip.css('top', event.clientY - 10);
                            tooltip.css('left', event.clientX + 40);
                        } else {
                            tooltip.css('top', event.clientY - 10);
                            tooltip.css('left', event.clientX - 20 - width);
                        }
                    });
                    horizontalPieData.mouseleave(function (event) {
                        $scope.width = 0;
                        var target = $(event.target);

                        // handle the case we 'stepped' on the tooltip
                        if (!target.hasClass('horizontal-pie-data')) {
                            target = target.parents('.horizontal-pie-data');
                        }
                        target.off('mouvemove');
                        target.children('.horizontal-pie-tooltip').first().css('display', 'none');
                    });
                };

                this.$onInit = function () {
                    if (!this.options || !this.data) {
                        console.error('No options or no data passed.');
                        return;
                    }

                    parseOptions();
                    normalizeData();

                    $scope.width = 0;
                    $timeout(initTooltip);

                    $scope.$watch(function () { return self.data; }, normalizeData, true);
                };
            }
        ],
        bindings: {
            options: '<',
            data: '<'
        }
});

},{}],2:[function(require,module,exports){
/**
 * Created by richard on 17/10/16.
 */

angular.module('bitcraft-horizontal-pie-chart', []);

},{}],3:[function(require,module,exports){
'use strict';
require('./horizontal-pie-chart.module.js');
require('./horizontal-pie-chart.component.js');

},{"./horizontal-pie-chart.component.js":1,"./horizontal-pie-chart.module.js":2}]},{},[3]);
