#!/usr/bin/env bash

OUTPUT_PATH=$(grep '"name"' bower.json | sed 's|[^"]*"name"[^"]*"\([^"]*\)"[^"]*|\1|')
bash bower_components/angular-utils/build.sh "$OUTPUT_PATH.js"
